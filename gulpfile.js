let gulp    = require('gulp'),
    chalk   = require('chalk'),
    express = require('express'),
    app     = express(),
    bw      = require('browser-sync');

gulp.task('publish', (done)=>{
    gulp.src(['src/*.html', 'src/*.jpg', 'src/*.css'])
        .pipe(gulp.dest('public'));
        console.log(chalk.blueBright('***** Archivos publicos copiados *******'));
        done();
});


gulp.task('server', ()=>{
    app.use(express.static('public'));
    app.listen(4001);
    console.log(chalk.redBright('*****   Servidor corriendo   *******'));
    console.log(chalk.redBright('*****   watch activo   *******'));
    gulp.watch(['src/*.html','src/*.css','./*.js'],gulp.series('publish','server'));
    //bw({ proxy: 'localhost:9001'});
});

gulp.task('default',gulp.series('publish','server'), ()=>{});